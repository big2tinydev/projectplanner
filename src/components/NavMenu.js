import React from 'react';
import {Button, Form, FormControl, Nav, Navbar, NavDropdown} from "react-bootstrap";


function NavMenu(props) {
    return (
        <Navbar fixed="top" bg="light" expand="lg">
            <Navbar.Brand href="/">Big2TinyDev</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/">About</Nav.Link>
                    <Nav.Link href="/">Contact</Nav.Link>

                    <NavDropdown title="Users" id="basic-nav-dropdown">
                        <NavDropdown.Item href="#register">Register</NavDropdown.Item>
                        <NavDropdown.Item href="#login">Login</NavDropdown.Item>
                        <NavDropdown.Item href="#logout">Logout</NavDropdown.Item>
                        <NavDropdown.Divider/>
                        <NavDropdown.Item href="#profile">Profile</NavDropdown.Item>
                    </NavDropdown>

                    <NavDropdown title="Dev Links" id="basic-nav-dropdown">
                        <NavDropdown.Item target="_blank" href="https://reactjs.org/docs/getting-started.html">React</NavDropdown.Item>
                        <NavDropdown.Divider/>
                        <NavDropdown.Item target="_blank" href="https://github.com/dannyconnell/localbase">Localbase</NavDropdown.Item>
                        <NavDropdown.Divider/>
                        <NavDropdown.Item target="_blank" href="https://strapi.io/documentation/v3.x/getting-started/quick-start.html">Strapi</NavDropdown.Item>
                        <NavDropdown.Divider/>
                        <NavDropdown.Item target="_blank" href="https://react-bootstrap.netlify.app/components/alerts/">Bootstrap</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2"/>
                    <Button variant="outline-success">Search</Button>
                </Form>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default NavMenu;